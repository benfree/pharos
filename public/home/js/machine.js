// 获取一级省份
var province = "";
//地图初始化时，在地图上添加一个marker标记,鼠标点击marker可弹出自定义的信息窗体
var map = new AMap.Map("container", {
    resizeEnable: true,
    // center: [121.465936,29.81167],
    zoom: 8
});
// 中国地图
// 路径配置
require.config({
    paths: {
        echarts: 'http://echarts.baidu.com/build/dist'
    }
});
// 使用
require(['echarts','echarts/chart/map'],function (ch) {
    // 基于准备好的dom，初始化echarts图表
    var myChart = ch.init(document.getElementById('main'));
    option = {
        tooltip : {
            trigger: 'item',
            formatter: '{b}'
        },
        series : [{
            name: '中国',
            type: 'map',
            mapType: 'china',
            selectedMode : 'multiple',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true},color:"#d43d3d"}
            },
            data:[
                {name:'浙江',selected:true}
            ]
        }]
    };

    var ecConfig = require('echarts/config');
    myChart.on(ecConfig.EVENT.MAP_SELECTED, function (param){
        province = param.target;
        if (province == "浙江") {
            $("#main").hide();
            $("#container").show();
        }/*else if (province == "北京") {
             map = new AMap.Map("container", {
             resizeEnable: true,
             center: [116.388677,39.929683],
             zoom: 8
             });
             }*/
    });
    // 为echarts对象加载数据
    myChart.setOption(option);
});


$.ajax({
    type: "post",
    url: "/home/index/map",
    success: function (data) {
        var date = JSON.parse(data);
        map.clearMap();
        var arr = [];
        var infoWindow;
        var icon;
        for (var i = 0; i < date.length; i++) {
            var isLogin = date[i].is_login;
            if (isLogin == 0) {
                icon = "/home/images/map.png"
            }else {
                icon = "/home/images/map1.png"
            }
            var marker = new AMap.Marker({
                map: map,
                position: [date[i].lat,date[i].lng],
                icon: icon,
                i: i
            });
            //实例化信息窗体
            var title = date[i].name,
                intro = date[i].introduction,
                add = date[i].address;
            content = [],
                content.push("内容简介：" + intro);
            var address = "具体地址：" + add;
            infoWindow = new AMap.InfoWindow({
                isCustom: true,  //使用自定义窗体
                content: createInfoWindow(title, content.join("<br/>"),address),

                offset: new AMap.Pixel(16, -45)
            });
            arr.push(infoWindow);
            //鼠标点击marker弹出自定义的信息窗体
            AMap.event.addListener(marker, 'click', function() {
                var t = this.F.i;
                console.log(t);

                $('#iframe-modao').show();
                $('#iframe-vr').show();
                // arr[t].open(map, this.getPosition());
                $("#my-video").css('display','none');
                // $("#iframe-phone").css('display','none');

                if (t === 1) {
                    $('#Vr').attr('src','https://720yun.com/t/5ecjtrekea8?scene_id=12729970');
                } else if (t === 2) {
                    $('#Vr').attr('src','https://720yun.com/t/5e4jezwy5u9?scene_id=17037009');
                } else {
                    $('#Vr').attr('src','https://720yun.com/t/0e5j57satf6?scene_id=12607075');
                }

                // 这个浣东
                // $('#Vr').attr('src','https://720yun.com/t/5ecjtrekea8?scene_id=12729970');
                // // 这个天津
                // $('#Vr').attr('src','https://720yun.com/t/5e4jezwy5u9?scene_id=17037009');
                // // 这个箬横
                // $('#Vr').attr('src','https://720yun.com/t/0e5j57satf6?scene_id=12607075');

            });
        }
    }
});

//构建自定义信息窗体
function createInfoWindow(title, content,address) {
    var info = document.createElement("div");
    info.className = "info";

    //可以通过下面的方式修改自定义窗体的宽高
    //info.style.width = "400px";
    // 定义顶部标题
    var top = document.createElement("div");
    var titleD = document.createElement("div");
    var closeX = document.createElement("img");
    top.className = "info-top";
    titleD.innerHTML = title;
    closeX.src = "http://webapi.amap.com/images/close2.gif";
    closeX.onclick = closeInfoWindow;

    top.appendChild(titleD);
    top.appendChild(closeX);
    info.appendChild(top);

    // 定义中部内容

    var addr = document.createElement("div");
    addr.className = "info-address";
    addr.style.backgroundColor = 'white';
    addr.innerHTML = address;
    info.appendChild(addr);

    var middle = document.createElement("div");
    middle.className = "info-middle";
    middle.style.backgroundColor = 'white';
    middle.innerHTML = content;
    info.appendChild(middle);

    // 定义底部内容
    var bottom = document.createElement("div");
    bottom.className = "info-bottom";
    bottom.style.position = 'relative';
    bottom.style.top = '0px';
    bottom.style.margin = '0 auto';
    var sharp = document.createElement("img");
    sharp.src = "http://webapi.amap.com/images/sharp.png";
    bottom.appendChild(sharp);
    info.appendChild(bottom);
    return info;
}

//关闭信息窗体
function closeInfoWindow() {
    map.clearInfoWindow();
}
