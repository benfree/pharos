<?php
/**
 * Created by PhpStorm.
 * User: Lxx<779219930@qq.com>
 * Date: 2018/3/16
 * Time: 16:46
 */

namespace app\admin\controller;


use think\Controller;

/**
 * Class Pharos
 * @package app\admin\controller
 * 灯塔内容管理
 */
class Pharos extends Admin {
    /**
     * 主页
     */
    public function index() {
        $map = array(
            'status' => 1,
        );
        $list = $this->lists('Pharos',$map);
        int_to_string($list, array(
            'tier' => array(1=>'市/县',2=>'乡镇',3=>'街道/村',4=>'两新组织',5=>'特色小镇',6=>'科研院所',7=>'其它'),
            'type' => array(1=>'企业号',2=>'订阅号',3=>'服务号'),
        ));
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 新增
     */
    public function add() {
        if(IS_POST) {


        }else {
            $this->assign('msg','');
            return $this->fetch('edit');
        }
    }

    /**
     * 编辑
     */
    public function edit() {
        if(IS_POST) {

        }else {

            return $this->fetch();
        }
    }

    /**
     * 删除
     */
    public function del() {

        return $this->fetch();
    }

}