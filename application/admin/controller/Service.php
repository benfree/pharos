<?php
/**
 * Created by PhpStorm.
 * User: 虚空之翼 <183700295@qq.com>
 * Date: 16/3/25
 * Time: 09:12
 * 微信事件接口
 */
namespace app\admin\controller;

use app\admin\model\Pharos;
use app\admin\model\WechatLog;
use app\admin\model\WechatUser;
use com\wechat\QYWechat;
use com\wechat\TPQYWechat;
use com\wechat\TPWechat;
use think\Config;
use think\Controller;
use think\Db;
use think\Log;

class Service extends Controller
{

    // 服务号接收的应用
    public function event() {
        $Wechat = new TPQYWechat(Config::get('work'));
        $res = $Wechat->valid();
//        Log::record("回调：".$res);

        $type = $Wechat->getRev()->getRevType();
        switch ($type) {
            case QYWechat::MSGTYPE_TEXT:
                $Wechat->text("您好！感谢关注！")->reply();
                break;
            case QYWechat::MSGTYPE_EVENT:
                $event = $Wechat->getRev()->getRevEvent();
                switch ($event['event']) {
                    case 'subscribe':
                        $replyText = "您好！欢迎关注新市大脚掌！";
                        $Wechat->text($replyText)->reply();
//                        $newsData = array(
//                            '0'=> array(
//                                'Title' => "欢迎您关注“新市大脚掌”",
//                                'Description' => "内含企业二维码，可转发给同事关注",
//                                'PicUrl' => "http://xspb.0571ztnet.com/home/images/special/music_2.jpg",
//                                'Url' => "http://u3665579.viewer.maka.im/pcviewer/WRZ0GJFA",
//                            ),
//                        );
//                        $Wechat->news($newsData)->reply();
                        break;
                    case 'enter_agent':
                        $data = array(
                            'event' => $event['event'],
                            'msgtype' => $type,
                            'agentid' => $Wechat->getRev()->getRevAgentID(),
                            'create_time' => $Wechat->getRev()->getRevCtime(),
                            'event_key' => isset($event['key']) ? $event['key'] : '',
                            'userid' => $Wechat->getRev()->getRevFrom()
                        );
//                        Log::record("进入事件：".json_encode($data));
                        $id  = WechatLog::create($data);
//                        Log::record("创建记录：".$id);
                        //$Wechat->text(json_encode($data))->reply();
                        save_log($Wechat->getRev()->getRevFrom(), 'WechatLog');
                        break;
                }
                break;
            case QYWechat::MSGTYPE_IMAGE:
                break;
            default:
                $Wechat->text("您好！感谢关注！")->reply();
        }

    }

    /**
     * 同步数据
     * TODO 每天定时调用此接口同步数据
     */
    public function sync(){
        //梦想小镇党群驿站
        $mxxzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/mxxzpb");
        $mxxzdata = $this->getdata($mxxzpb, 1);

        //河南邓州古城街道党建
        $dzgcpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/dzgcpb");
        $dzgcdata = $this->getdata($dzgcpb, 2);

        //诸暨市山下湖镇智慧党建
        $zzxzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/zzxzpb");
        $zzxzdata = $this->getdata($zzxzpb, 3);

        //宁波市规划与地理信息中心
        $partybuildnbghj = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/partybuildnbghj");
        $partybuildnbghjdata = $this->getdata($partybuildnbghj, 4);

        //红绣浣东
        $partybuildzjhx = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/partybuildzjhx");
        $partybuildzjhxdata = $this->getdata($partybuildzjhx, 5);

        //两美箬横
        $rhpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/rhpb");
        $rhpbdata = $this->getdata($rhpb, 6);

        //石淙党建
        $szzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/szzpb");
        $szzdata = $this->getdata($szzpb, 7);

        //台州市个协党建
        $tzgxpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/tzgxpb");
        $tzgxdata = $this->getdata($tzgxpb, 8);

        //台州红云 无字段 不需要同步
        $tzsz = db(1,"mysql://ztrun:db0219$%@111.3.65.60:3630/tzsz");
        $tzszdata = $this->getdata($tzsz, 9, true);

        //中国移动台州分公司
        $tzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/tzpb");
        $tzdata = $this->getdata($tzpb, 10);

        //金清党员之家
        $jqzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/jqzpb");
        $jqzdata = $this->getdata($jqzpb, 11);

        //中科院光电院对地观测部党支部
        $zkypb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/zkypb");
        $zkydata = $this->getdata($zkypb, 12);

        //镇西村党建
        $sczdyzx = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/sczdyzx");
        $sczdyzxdata = $this->getdata($sczdyzx, 13);

        //建德城投智慧党建
        $partybuildct = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/partybuildct");
        $partybuildctdata = $this->getdata($partybuildct, 14);

        //智慧莫干
        $mgspb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/mgspb");
        $mgsdata = $this->getdata($mgspb, 15);

        //德清地信小镇党建
        $dqpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/dqpb");
        $dqdata = $this->getdata($dqpb, 16);

        //香市党建
        $xspb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/xspb");
        $xsdata = $this->getdata($xspb, 17);

        //浙江机关党建 无字段 不需要同步
        $partybuild = db(1,"mysql://root:ztdb^&*0319@61.147.197.121:3630/partybuild");
        $partybuilddata = $this->getdata($partybuild, 18, true);

        //新市大脚掌
        $djzpb = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/djzpb");
        $djzdata = $this->getdata($djzpb, 19);

        //信达教育党建
        $yhxd = db(1,"mysql://ztrun:db0219$%@116.62.163.231:3630/yhxd");
        $yhxddata = $this->getdata($yhxd, 20);


        //TODO 调用新项目接口同步数据


    }
    /**
     * 录入到本地数据库
     */
    public function setdata($data, $id){
        return Pharos::update($data,['id'=>$id]);
    }
    /**
     * 处理数据
     */
    public function getdata($db, $id, $type=false){
        $data['sum'] = $db->table('pb_wechat_user')->count();    //总人数
        //统计性别
        $data['male'] = $db->table('pb_wechat_user')->where('gender', 'eq', 1)->count();      //男性人数
        $data['female'] = $db->table('pb_wechat_user')->where('gender', 'eq', 2)->count();    //女性人数
        if ($type) { // 无字段 不需要同步
            $this->setdata($data, $id);
            return $data;
        }

        //统计学历
        $data['edu1'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "小学"], ["eq", "高小"], ["eq", "初中以下"], 'or']])->count();    //初中以下
        $data['edu2'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "初中"], ["eq", "初小"], 'or']])->count();    //初中
        $data['edu3'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "高中"], ["eq", "普通高中"], 'or']])->count();    //高中
        $data['edu4'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "中专"], ["eq", "中等专科"], ["eq", "职高"], ["eq", "职业高中"], ["eq", "技工学校"], ["eq", "技校"], 'or']])->count();    //中专
        $data['edu5'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "大专"], ["eq", "大学专科"], ["eq", "专科"], ["eq", "党校大专"], ["eq", "中央党校大专"], ["eq", "省（区、市）委党校大专"], 'or']])->count();    //大专
        $data['edu6'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "本科"], ["eq", "大学"], ["eq", "大学本科"], ["eq", "大学普通班"], ["eq", "党校大学"], ["eq", "中央党校大学"], 'or']])->count();    //本科
        $data['edu7'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "硕士"], ["eq", "研究生"], ["eq", "硕士研究生"], ["eq", "党校研究生"], ["eq", "中央党校研究生"], 'or']])->count();    //硕士
        $data['edu8'] = $db->table('pb_wechat_user')->where(['education' => [["eq", "硕士以上"], ["eq", "博士"], ["eq", "博士及以上"], ["eq", "博士研究生"], 'or']])->count();    //硕士以上

        $result = $db->table('pb_wechat_user')->field('id, birthday, partytime')->select();
//        var_dump($result);die;
        $data['age1'] = 0;
        $data['age2'] = 0;
        $data['age3'] = 0;
        $data['age4'] = 0;
        $data['age5'] = 0;
        $data['party1'] = 0;
        $data['party2'] = 0;
        $data['party3'] = 0;
        $data['party4'] = 0;
        $data['party5'] = 0;
        $data['party6'] = 0;
        if ($result) {
            foreach ($result as $value) {
                $value['birthday'] = str_replace('.', '-', $value['birthday']);
                $value['birthday'] = str_replace('年', '-', $value['birthday']);
                $value['birthday'] = str_replace('月', '-', $value['birthday']);
                $value['birthday'] = str_replace('日', '', $value['birthday']);
                $value['partytime'] = str_replace('.', '-', $value['partytime']);
                $value['partytime'] = str_replace('年', '-', $value['partytime']);
                $value['partytime'] = str_replace('月', '-', $value['partytime']);
                $value['partytime'] = str_replace('日', '', $value['partytime']);
                if ($age=strtotime($value['birthday'])) {
                    $age = floor((time()-$age)/86400/365);
                    if ($age >= 18 && $age <= 28) {
                        $data['age1']++;
                    } elseif ($age > 28 && $age <= 40) {
                        $data['age2']++;
                    } elseif ($age > 40 && $age <= 55) {
                        $data['age3']++;
                    } elseif ($age > 55 && $age <= 70) {
                        $data['age4']++;
                    } elseif ($age > 70) {
                        $data['age5']++;
                    }
                }
                if ($party=strtotime($value['partytime'])) {
                    $party = floor((time()-$party)/86400/365);
                    if ($party < 1) {
                        $data['party1']++;
                    } elseif ($party >= 1 && $party < 5) {
                        $data['party2']++;
                    } elseif ($party >= 5 && $party < 10) {
                        $data['party3']++;
                    } elseif ($party >= 10 && $party < 20) {
                        $data['party4']++;
                    } elseif ($party >= 20 && $party < 40) {
                        $data['party5']++;
                    } elseif ($party >= 40) {
                        $data['party6']++;
                    }
                }

            }
        }
        $this->setdata($data, $id);

        return $data;
        //统计年龄
        /*$data['age1'] = $db->table('pb_wechat_user')->where('timestampdiff(year, birthday, now())>=18 and timestampdiff(year, birthday, now())<=28')->count();  //18-28
        $data['age2'] = $db->table('pb_wechat_user')->where('timestampdiff(year, birthday, now())>28 and timestampdiff(year, birthday, now())<=40')->count();    //28-40
        $data['age3'] = $db->table('pb_wechat_user')->where('timestampdiff(year, birthday, now())>40 and timestampdiff(year, birthday, now())<=55')->count();    //40-55
        $data['age4'] = $db->table('pb_wechat_user')->where('timestampdiff(year, birthday, now())>55 and timestampdiff(year, birthday, now())<=70')->count();    //55-70
        $data['age5'] = $db->table('pb_wechat_user')->where('timestampdiff(year, birthday, now())>70')->count();    //70以上*/

        //统计党龄
        /*$data['party1'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())<1')->count();  //0-1
        $data['party2'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())>=1 and timestampdiff(year, partytime, now())<5')->count();    //1-5
        $data['party3'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())>=5 and timestampdiff(year, partytime, now())<10')->count();    //5-10
        $data['party4'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())>=10 and timestampdiff(year, partytime, now())<20')->count();    //10-20
        $data['party5'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())>=20 and timestampdiff(year, partytime, now())<40')->count();    //20-40
        $data['party6'] = $db->table('pb_wechat_user')->where('timestampdiff(year, partytime, now())>=40');//->count();    //40以上
        var_dump($data['party6']->fetchSql()->select());die;*/

    }

}