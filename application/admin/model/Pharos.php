<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2018/3/21
 * Time: 9:43
 */

namespace app\admin\model;


use think\Model;

class Pharos extends Base
{
    protected $insert = [
        'create_time' => NOW_TIME,
        'status' => 1
    ];
    
    //获取data数据表信息
    public function getMsg() {
        return $this->hasOne("PharosData","pharos_id","id");
    }
}